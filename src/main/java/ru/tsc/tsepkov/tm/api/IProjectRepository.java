package ru.tsc.tsepkov.tm.api;

import ru.tsc.tsepkov.tm.model.Project;
import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    List<Project> findAll();

    void clear();

}
