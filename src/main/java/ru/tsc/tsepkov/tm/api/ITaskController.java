package ru.tsc.tsepkov.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

}
