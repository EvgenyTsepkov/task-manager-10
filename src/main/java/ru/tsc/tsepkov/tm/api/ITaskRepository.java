package ru.tsc.tsepkov.tm.api;

import ru.tsc.tsepkov.tm.model.Task;
import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    List<Task> findAll();

    void clear();

}
