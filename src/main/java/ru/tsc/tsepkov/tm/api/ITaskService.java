package ru.tsc.tsepkov.tm.api;

import ru.tsc.tsepkov.tm.model.Task;

public interface ITaskService extends ITaskRepository{

    Task create(String name, String description);

}
