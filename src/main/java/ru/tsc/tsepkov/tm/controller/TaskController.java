package ru.tsc.tsepkov.tm.controller;

import ru.tsc.tsepkov.tm.api.ITaskController;
import ru.tsc.tsepkov.tm.api.ITaskService;
import ru.tsc.tsepkov.tm.model.Task;
import ru.tsc.tsepkov.tm.util.TerminalUtil;
import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        int index = 1;
        final List<Task> tasks = taskService.findAll();
        for (final Task task: tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

}
