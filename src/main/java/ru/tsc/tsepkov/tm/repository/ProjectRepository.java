package ru.tsc.tsepkov.tm.repository;

import ru.tsc.tsepkov.tm.api.IProjectRepository;
import ru.tsc.tsepkov.tm.model.Project;
import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void clear() {
        projects.clear();
    }

}
