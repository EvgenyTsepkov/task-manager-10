package ru.tsc.tsepkov.tm.repository;

import ru.tsc.tsepkov.tm.api.ITaskRepository;
import ru.tsc.tsepkov.tm.model.Task;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(Task task) {
        tasks.add(task);
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

}
